<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Scratch
 *
 * @ORM\Table(name="scratch")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ScratchRepository")
 *
 */
class Scratch
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $itemName
     *
     * @ORM\Column(type = "string", length = 40, nullable=false)
     * @Assert\NotBlank(message="Is required")
     */
    private $itemName;

    /**
     * @var string $itemDescription
     *
     * @ORM\Column(type="text", nullable=false)
     */
    private $itemDescription;

    /**
     * @var string $itemSize
     *
     * @ORM\Column(type = "string", length = 40, nullable=false)
     * @Assert\NotBlank(message="Is required")
     */
    private $itemSize;

    /**
     * @var string $itemCost
     *
     * @ORM\Column(type = "float", nullable=false)
     * @Assert\NotBlank(message="Price is required")
     */
    private $itemCost;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Scratch
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getItemName()
    {
        return $this->itemName;
    }

    /**
     * @param string $itemName
     *
     * @return Scratch
     */
    public function setItemName($itemName)
    {
        $this->itemName = $itemName;

        return $this;
    }

    /**
     * @return string
     */
    public function getItemDescription()
    {
        return $this->itemDescription;
    }

    /**
     * @param string $itemDescription
     *
     * @return Scratch
     */
    public function setItemDescription($itemDescription)
    {
        $this->itemDescription = $itemDescription;

        return $this;
    }

    /**
     * @return string
     */
    public function getItemSize()
    {
        return $this->itemSize;
    }

    /**
     * @param string $itemSize
     *
     * @return Scratch
     */
    public function setItemSize($itemSize)
    {
        $this->itemSize = $itemSize;

        return $this;
    }

    /**
     * @return string
     */
    public function getItemCost()
    {
        return $this->itemCost;
    }

    /**
     * @param string $itemCost
     *
     * @return Scratch
     */
    public function setItemCost($itemCost)
    {
        $this->itemCost = $itemCost;

        return $this;
    }
}
