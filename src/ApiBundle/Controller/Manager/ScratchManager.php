<?php

namespace ApiBundle\Controller\Manager;

use ApiBundle\Controller\BaseApiController;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Symfony\Component\HttpFoundation\Request;

class ScratchManager extends BaseApiController
{
    /**
     * @param Request $request
     *
     * @return string
     */
    public function listAll(Request $request)
    {
        $data = $this->getDoctrine()->getRepository('AppBundle:Scratch')->findAll();

        $resource = new Collection(
            $data, function ($item) {
                return array(
                    'id' => $item->getId(),
                    'item_name' => $item->getItemName(),
                    'item_description' => $item->getItemDescription(),
                    'item_size' => $item->getItemSize(),
                    'item_cost' => $item->getItemCost(),
                );
            }
        );

        return $this->manager->createData($resource)->toJson();
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    public function item(Request $request)
    {
        $data = $this->findById($request->attributes->get('id'));

        $resource = new Item($data, function ($item) {
                return array(
                    'id' => $item->getId(),
                    'item_name' => $item->getItemName(),
                    'item_description' => $item->getItemDescription(),
                    'item_size' => $item->getItemSize(),
                    'item_cost' => $item->getItemCost(),
                );
            }
        );

        return $this->manager->createData($resource)->toJson();
    }

    /**
     * @param $id
     *
     * @return \AppBundle\Entity\Scratch|null|object
     * @throws \Exception
     */
    public function findById($id)
    {
        $scratch = $this->getDoctrine()->getRepository('AppBundle:Scratch')
            ->find($id);

        if (! $scratch ) {
            throw new \Exception('Item not found', 404);
        }

        return $scratch;
    }
}
