<?php
namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use League\Fractal;
use League\Fractal\Manager;
use League\Fractal\Serializer\DataArraySerializer;

class BaseApiController extends Controller
{
    /** @var Manager  */
    protected $manager;

    public function __construct()
    {
        $this->manager = new Manager();
        $this->manager->setSerializer(new DataArraySerializer());
    }

    /**
     * @param        $data
     * @param        $currentCursor
     * @param        $previousCursor
     * @param string $reference
     *
     * @return Fractal\Pagination\Cursor
     */
    public function createCursor($data, $currentCursor, $previousCursor, $reference = 'id')
    {
        $newCursor = is_null($data['data']) ? null : end($data['data']);
        $cursor = new Fractal\Pagination\Cursor(
            $currentCursor,
            $previousCursor,
            $newCursor ? $newCursor[$reference] : null,
            count($data['data'])
        );

        return $cursor;
    }

    /**
     * @param $url
     *
     * @return null
     */
    protected function getFilters($url)
    {
        $query = null;
        $parts = parse_url($url);
        if (isset($parts['query'])) {
            parse_str($parts['query'], $query);
        }

        return $query;
    }
}
