<?php

namespace ApiBundle\Controller;

use ApiBundle\Controller\Manager\ScratchManager;
use AppBundle\Entity\Scratch;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ScratchController
 *
 * @package ApiBundle\Controller
 */
class ScratchController extends ScratchManager
{
    /**
     * @Route("/overview", name="_api_overview")
     */
    public function indexAction()
    {
        return $this->render('ApiBundle:Default:index.html.twig');
    }

    /**
     * @param Request $request
     * @Route("/scratches", name="_get_all_scratches")
     * @Method({"GET"})
     *
     * @return JsonResponse|Response
     */
    public function listAction(Request $request)
    {
        try {
            return new Response(
                $this->listAll($request),
                200,
                array('Content-Type' => 'application/json')
            );
        } catch (\Exception $e) {
            $code = $e->getCode();
            $data = array(
                'error' => array(
                    'code' => $code,
                    'message' => $e->getMessage()
                )
            );
            return new JsonResponse($data, $code);
        }
    }

    /**
     * @param Request $request
     * @Route("/scratches", name="_new_scratch")
     * @Method({"POST"})
     *
     * @return JsonResponse
     */
    public function newAction(Request $request)
    {
        try {
            $data = $request->request->all();

            if (
                ! isset($data['name']) ||
                ! isset($data['description']) ||
                ! isset($data['size']) ||
                ! isset($data['price']))
            {
                throw new \Exception('Incorrect data, see documentation', 400);
            }

            $scratch = new Scratch();
            $scratch->setItemName($data['name']);
            $scratch->setItemDescription($data['description']);
            $scratch->setItemSize($data['size']);
            $scratch->setItemCost($data['price']);

            $this->getDoctrine()->getManager()->persist($scratch);
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(array(
                'success' => array(
                    'code' => 201,
                    'message' => 'Item added successfully'
                )
            ));

        } catch (\Exception $e) {
            $code = $e->getCode();
            $data = array(
                'error' => array(
                'code' => $code,
                'message' => $e->getMessage()
                )
            );

            return new JsonResponse($data, $code);
        }
    }

    /**
     * @param Request $request
     * @Route("/scratches/{id}", name="_get_scratch")
     * @Method({"GET"})
     *
     * @return JsonResponse|Response
     */
    public function getAction(Request $request)
    {
        try {
            return new Response(
                $this->item($request),
                200,
                array('Content-Type' => 'application/json')
            );
        } catch (\Exception $e) {
            $code = $e->getCode();
            $data = array(
                'error' => array(
                    'code' => $code,
                    'message' => $e->getMessage()
                )
            );
            return new JsonResponse($data, $code);
        }
    }

    /**
     * @param Request $request
     * @Route("/scratches/{id}", name="_update_scratch")
     * @Method({"PUT"})
     *
     * @return JsonResponse
     */
    public function updateAction(Request $request)
    {
        try {

            $scratch = $this->findById($request->attributes->get('id'));

            if (
                ! $request->get('name') ||
                ! $request->get('description') ||
                ! $request->get('size') ||
                ! $request->get('price')
            ) {
                throw new \Exception('Incorrect data, see documentation', 400);
            }

            $scratch->setItemName($request->get('name'));
            $scratch->setItemDescription($request->get('description'));
            $scratch->setItemSize($request->get('size'));
            $scratch->setItemCost($request->get('price'));

            $this->getDoctrine()->getManager()->persist($scratch);
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(array(
                'success' => array(
                    'code' => 204,
                    'message' => 'Item updated successfully'
                )
            ));

        } catch (\Exception $e) {
            $code = $e->getCode();
            $data = array(
                'error' => array(
                    'code' => $code,
                    'message' => $e->getMessage()
                )
            );

            return new JsonResponse($data, $code);
        }
    }

    /**
     * @param Request $request
     * @Route("/scratches/{id}/remove", name="delete_scratch")
     * @Method({"DELETE"})
     *
     * @return JsonResponse
     */
    public function deleteAction(Request $request)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $scratch = $this->findById($request->attributes->get('id'));

            $em->remove($scratch);
            $em->flush();

            return new JsonResponse(array(
                'success' => array(
                    'code' => 201,
                    'message' => 'Item deleted'
                )
            ));

        } catch (\Exception $e) {
            $code = $e->getCode();
            $data = array(
                'error' => array(
                    'code' => $code,
                    'message' => $e->getMessage()
                )
            );

            return new JsonResponse($data, $code);
        }
    }
}
